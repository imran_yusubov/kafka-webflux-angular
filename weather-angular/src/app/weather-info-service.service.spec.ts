import { TestBed } from '@angular/core/testing';

import { WeatherInfoServiceService } from './weather-info-service.service';

describe('WeatherInfoServiceService', () => {
  let service: WeatherInfoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeatherInfoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
